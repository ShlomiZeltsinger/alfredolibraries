pragma solidity ^0.4.11;


import "./LibNoProxy.sol";

library LibOne {

  function setUint(LibNoProxy.Data storage data, uint _num) public {
  	data.num = _num;
  }
}