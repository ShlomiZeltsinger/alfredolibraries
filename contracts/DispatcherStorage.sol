pragma solidity ^0.4.11;

contract DispatcherStorage {
  address public lib;
  mapping(bytes4 => uint32) public sizes;

  function DispatcherStorage(address newLib) {
    sizes[bytes4(0x41b970a3)] = 32;	
    replace(newLib);
  }

  function replace(address newLib){
    lib = newLib;
  }
}