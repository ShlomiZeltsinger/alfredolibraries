pragma solidity ^0.4.11;

library LibNoProxy {
  struct Data { 
  	uint num;
  	}

  function setUint(Data storage data, uint _num) public;
}