pragma solidity ^0.4.11;

import "./LibNoProxy.sol";

contract TheContract {
  LibNoProxy.Data data;

  using LibNoProxy for LibNoProxy.Data;

  function TheContract(){
  	data.num = 19;
  }

  function get() constant public returns (uint) {
    return data.num;
  }

  function set(uint _num) {
    return data.setUint(_num);
  }
}