pragma solidity ^0.4.11;


import "./LibNoProxy.sol";


library LibTwo {

  function setUint(LibNoProxy.Data storage data, uint _num) public {
  	data.num = _num * 2;
  }
}