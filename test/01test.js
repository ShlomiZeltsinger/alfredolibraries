TheContract = artifacts.require("TheContract");
Dispatcher = artifacts.require("Dispatcher");
LibNoProxy = artifacts.require("LibNoProxy");
LibOne = artifacts.require("LibOne");
LibTwo = artifacts.require("LibTwo");
DispatcherStorage = artifacts.require("DispatcherStorage");

expect = require("chai").expect;

contract("Test file one", function(accounts){
	describe("Deploys and grabs the two librares", function(){
		it("Should deploy and grab LibOne", function(){
			return LibOne.new().then((res)=>{
				libOne = res;
				expect(libOne).to.not.be.an("Error");
			});
		});
		it("Should deploy and grab LibTwo", function(){
			return LibTwo.new().then((res)=>{
				libTwo = res;
				expect(libTwo).to.not.be.an("Error");
			});
		});
		it("Should deploy and grab DispatcherStorage", function(){
			return DispatcherStorage.new(libOne.address).then((res)=>{
				dispatcherStorage = res;
				expect(dispatcherStorage).to.not.be.an("Error");
			});
		});
	});

	describe("Deploys and grabs Dispatcher and TheContract", function(){
		it("Should deploy and grab Dispatcher", function(){
          	Dispatcher.unlinked_binary = Dispatcher.unlinked_binary
            .replace('1111222233334444555566667777888899990000',
            dispatcherStorage.address.slice(2));
			return Dispatcher.new().then((res)=>{
				dispatcher = res;
				expect(dispatcher).to.not.be.an("Error");
			});
		});
		it("Should deploy and grab TheContract", function(){
			TheContract.link("LibNoProxy", dispatcher.address);

			return TheContract.new().then((res)=>{
				theContract = res;
				expect(theContract).to.not.be.an("Error");		
			});
		});

		describe("Check the methods", function(){
			it("Should change library to LibOne on dispatcher", function(){
				return dispatcherStorage.replace(libOne.address).then(res => {
					expect(res).to.not.be.an("Error");
				});
			});
			it("should get 19 uint from TheContract", function(){
				return theContract.get().then(res => {
					expect(res.toNumber()).to.be.equal(19);
				});
			});
			it("Should set to 5", function(){
				return theContract.set(5);
			});
			it("should get 5 from TheContract", function(){
				return theContract.get().then(res => {
					expect(res.toNumber()).to.be.equal(5);
				});
			});
			it("Should change library to LibTwo on dispatcher", function(){
				return dispatcherStorage.replace(libTwo.address).then(res => {
					expect(res).to.not.be.an("Error");
				});
			});
			it("Should set to 7", function(){
				return theContract.set(7);
			});
			it("should get 14 from TheContract", function(){
				return theContract.get().then(res => {
					expect(res.toNumber()).to.be.equal(14);
				});
			});
		});
	});
});
